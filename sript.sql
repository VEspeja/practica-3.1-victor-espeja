create database liga;
use liga;

create table estadio(
id int auto_increment primary key,
nombre varchar(50) unique,
fecha_apertura date,
localidad_estadio varchar(50)
);

create table equipo(
id int primary key auto_increment,
nombre varchar(50),
id_estadio int,
localidad_equipo varchar(50),
foreign key (id_estadio) references estadio (id) on delete set null
);

create table entrenador(
id int primary key auto_increment,
nombre varchar(50),
anyos_trabajando int,
id_equipo int,
foreign key (id_equipo) references equipo (id) on delete set null
);


create table jugador(
id int primary key auto_increment,
nombre varchar(50),
dorsal int,
fecha_nacimiento date
);

create table entrenador_jugador(
id_jugador int,
id_entrenador int,
primary key(id_jugador,id_entrenador),
foreign key (id_jugador) references jugador (id),
foreign key (id_entrenador) references entrenador (id)
);

