package base;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.*;

@Entity
public class Jugador {
    private int id;
    private String nombre;
    private int dorsal;
    private Date fechaNacimiento;
    private Set<Entrenador> listaEntrenadores;

    public Jugador() {
        listaEntrenadores = new HashSet<>();
    }

    public Jugador(String nombre, int dorsal, LocalDate fechaNacimiento) {
        this.nombre = nombre;
        this.dorsal = dorsal;
        this.fechaNacimiento = Date.valueOf(fechaNacimiento);
        listaEntrenadores = new HashSet<>();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre", nullable = true, length = 50,unique = true)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "dorsal", nullable = true)
    public int getDorsal() {
        return dorsal;
    }

    public void setDorsal(int dorsal) {
        this.dorsal = dorsal;
    }

    @Basic
    @Column(name = "fecha_nacimiento", nullable = true)
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Jugador jugador = (Jugador) o;
        return id == jugador.id &&
                dorsal == jugador.dorsal &&
                Objects.equals(nombre, jugador.nombre) &&
                Objects.equals(fechaNacimiento, jugador.fechaNacimiento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, dorsal, fechaNacimiento);
    }

    @ManyToMany(mappedBy = "listaJugadores")
    public Set<Entrenador> getListaEntrenadores() {
        return listaEntrenadores;
    }

    public void setListaEntrenadores(Set<Entrenador> listaEntrenadores) {
        this.listaEntrenadores = listaEntrenadores;
    }

    @Override
    public String toString() {
        return id +" - "+ nombre +" - "+dorsal+" - "+fechaNacimiento;
    }

    public void asignarEntrenadores(Collection<Entrenador> entrenadores){
        for (Entrenador entrenador : entrenadores){
            entrenador.getListaJugadores().add(this);
        }
        this.listaEntrenadores.addAll(entrenadores);
    }

    public void desvincularEntrenadores(Collection<Entrenador> entrenadores){
        for (Entrenador entrenador : entrenadores){
            entrenador.getListaJugadores().remove(this);
        }
        this.listaEntrenadores.removeAll(entrenadores);
    }

    public void desvincularTodos(){
        for(Entrenador entrenador : listaEntrenadores){
            entrenador.getListaJugadores().remove(this);
        }
        listaEntrenadores.clear();
    }
}
