package base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Equipo {
    private int id;
    private String nombre;
    private String localidadEquipo;
    private Estadio estadio;
    private List<Entrenador> listaEntrenadores;

    public Equipo() {
        listaEntrenadores = new ArrayList<>();
    }

    public Equipo(String nombre, String localidadEquipo, Estadio estadio) {
        this.nombre = nombre;
        this.localidadEquipo = localidadEquipo;
        this.estadio = estadio;
        this.listaEntrenadores = new ArrayList<>();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre", nullable = true, length = 50)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "localidad_equipo", nullable = true, length = 50)
    public String getLocalidadEquipo() {
        return localidadEquipo;
    }

    public void setLocalidadEquipo(String localidadEquipo) {
        this.localidadEquipo = localidadEquipo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Equipo equipo = (Equipo) o;
        return id == equipo.id &&
                Objects.equals(nombre, equipo.nombre) &&
                Objects.equals(localidadEquipo, equipo.localidadEquipo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, localidadEquipo);
    }

    @ManyToOne
    @JoinColumn(name = "id_estadio", referencedColumnName = "id")
    public Estadio getEstadio() {
        return estadio;
    }

    public void setEstadio(Estadio estadio) {
        this.estadio = estadio;
    }

    @OneToMany
    @JoinColumn(name = "id_estadio", referencedColumnName = "id")
    public List<Entrenador> getListaEntrenadores() {
        return listaEntrenadores;
    }

    public void setListaEntrenadores(List<Entrenador> listaEntrenadores) {
        this.listaEntrenadores = listaEntrenadores;
    }

    @Override
    public String toString() {
        return id+" - "+nombre +" - "+localidadEquipo+" - "+estadio;
    }
}
