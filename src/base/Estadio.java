package base;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Estadio {
    private int id;
    private String nombre;
    private Date fechaApertura;
    private String localidadEstadio;
    private List<Equipo> listaEquipos;

    public Estadio() {
        listaEquipos = new ArrayList<>();
    }

    public Estadio(String nombre, LocalDate fechaApertura, String localidadEstadio) {
        this.nombre = nombre;
        this.fechaApertura = Date.valueOf(fechaApertura);
        this.localidadEstadio = localidadEstadio;
        listaEquipos = new ArrayList<>();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre", nullable = true, length = 50)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "fecha_apertura", nullable = true)
    public Date getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(Date fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    @Basic
    @Column(name = "localidad_estadio", nullable = true, length = 50)
    public String getLocalidadEstadio() {
        return localidadEstadio;
    }

    public void setLocalidadEstadio(String localidadEstadio) {
        this.localidadEstadio = localidadEstadio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Estadio estadio = (Estadio) o;
        return id == estadio.id &&
                Objects.equals(nombre, estadio.nombre) &&
                Objects.equals(fechaApertura, estadio.fechaApertura) &&
                Objects.equals(localidadEstadio, estadio.localidadEstadio);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, fechaApertura, localidadEstadio);
    }

    @OneToMany(mappedBy = "estadio")
    public List<Equipo> getListaEquipos() {
        return listaEquipos;
    }

    public void setListaEquipos(List<Equipo> listaEquipos) {
        this.listaEquipos = listaEquipos;
    }

    @Override
    public String toString() {
        return id+" - "+nombre +" - "+fechaApertura+" - "+ localidadEstadio;
    }
}
