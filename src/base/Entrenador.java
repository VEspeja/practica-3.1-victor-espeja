package base;

import javax.persistence.*;
import java.util.*;

@Entity
public class Entrenador {
    private int id;
    private String nombre;
    private int anyosTrabajando;
    private Set<Jugador> listaJugadores;
    private Equipo equipo;

    public Entrenador() {
        listaJugadores = new HashSet<>();
    }

    public Entrenador(String nombre, int anyosTrabajando, Equipo equipo) {
        this.nombre = nombre;
        this.anyosTrabajando = anyosTrabajando;
        this.equipo = equipo;
        this.listaJugadores = new HashSet<>();
    }

    public Entrenador(String nombre, int anyosTrabajando) {
        this.nombre = nombre;
        this.anyosTrabajando = anyosTrabajando;
        this.listaJugadores = new HashSet<>();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre", nullable = true, length = 50)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "anyos_trabajando", nullable = true)
    public int getAnyosTrabajando() {
        return anyosTrabajando;
    }

    public void setAnyosTrabajando(int anyosTrabajando) {
        this.anyosTrabajando = anyosTrabajando;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entrenador that = (Entrenador) o;
        return id == that.id &&
                anyosTrabajando == that.anyosTrabajando &&
                Objects.equals(nombre, that.nombre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, anyosTrabajando);
    }

    @ManyToMany
    @JoinTable(name = "entrenador_jugador", catalog = "", schema = "liga", joinColumns = @JoinColumn(name = "id_entrenador", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_jugador", referencedColumnName = "id", nullable = false))
    public Set<Jugador> getListaJugadores() {
        return listaJugadores;
    }

    public void setListaJugadores(Set<Jugador> listaJugadores) {
        this.listaJugadores = listaJugadores;
    }

    @ManyToOne
    @JoinColumn(name = "id_equipo", referencedColumnName = "id")
    public base.Equipo getEquipo() {
        return equipo;
    }

    public void setEquipo(base.Equipo equipo) {
        this.equipo = equipo;
    }

    @Override
    public String toString() {
        return id +" - "+ nombre +" - "+anyosTrabajando+" - Jugadores: "+listaJugadores;
    }

    public void asignarJugadores(Collection<Jugador> jugadores){
        for (Jugador jugador : jugadores){
            jugador.getListaEntrenadores().add(this);
        }
        this.listaJugadores.addAll(jugadores);
    }

    public void desvincularJugadores(Collection<Jugador> jugadores){
        for (Jugador jugador : jugadores){
            jugador.getListaEntrenadores().remove(this);
        }
        this.listaJugadores.removeAll(jugadores);
    }

    public void desvincularTodos(){
        for(Jugador jugador : listaJugadores){
            jugador.getListaEntrenadores().remove(this);
        }
        listaJugadores.clear();
    }
}
