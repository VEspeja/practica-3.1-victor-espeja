package MVC;

import base.Entrenador;
import base.Equipo;
import base.Estadio;
import base.Jugador;
import principal.Util;
import usuarios.Usuario;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Controlador implements ActionListener, ListSelectionListener, WindowListener {
    // creo un objeto de tipo Vista
    private Vista vista ;
    // creo un objeto de tipo Modelo
    private Modelo modelo;
    // creo un objeto de tipo VistaLogin
    private VistaLogin vistaLogin;
    private Util util;
    boolean contrasena;
    boolean usuario;
    Entrenador entrenador;
    Entrenador entrenadorJugador;
    Equipo equipo;
    Estadio estadio;
    Jugador jugador;
    ArrayList<Entrenador> listaEntrenadores;
    Actualizar hiloActualizar;

    public Controlador(Vista unaVista, Modelo unModelo, VistaLogin vistaLogin, Util unUtil) {
        // declaro la vista con la Vista que recibo como parametro
        this.vista = unaVista;
        vista.frame.setVisible(false);
        this.vistaLogin = vistaLogin;
        // declaro el modelo con el Modelo que recibo como parametro
        this.modelo = unModelo;

        // declaro el Util con el util que recibo como parametro
        this.util = unUtil;
        // llamo al metodo que se encarga de anyadir los ActionListeners necesarios
        anyadirActionListener(this);
        anyadirListSelectionListener(this);
        anyadirWindowListener(this);
        modelo.conectar();
        listarEstadios();
        listarEstadiosComboBox();
        listarEquipos();
        listarEquiposComboBox();
        listarEntrendores();
        hiloActualizar = new Actualizar();
        hiloActualizar.start();
        listaEntrenadores = new ArrayList<>();
    }

    private void anyadirWindowListener(WindowListener listener) {
        vista.frame.addWindowListener(listener);
    }

    private void anyadirListSelectionListener(ListSelectionListener listener) {
        vista.listaEstadio.addListSelectionListener(listener);
        vista.listaEquipo.addListSelectionListener(listener);
        vista.listaEntrenador.addListSelectionListener(listener);
        vista.listaJugadores.addListSelectionListener(listener);
        vista.listaEntrenadoresSinAsignar.addListSelectionListener(listener);
        vista.listaEntrenadoresAsignados.addListSelectionListener(listener);
        vista.listaJugadoresLibres.addListSelectionListener(listener);
        vista.listaJugadoresEntrenados.addListSelectionListener(listener);
    }

    private void anyadirActionListener(ActionListener listener) {
        vista.btnAltaEstadio.addActionListener(listener);
        vista.btnModificarEstadio.addActionListener(listener);
        vista.btnEliminarEstadio.addActionListener(listener);
        vista.btnAltaEquipo.addActionListener(listener);
        vista.btnModificarEquipo.addActionListener(listener);
        vista.btnEliminarEquipo.addActionListener(listener);
        vista.btnAltaEntrenador.addActionListener(listener);
        vista.btnModificarEntrenador.addActionListener(listener);
        vista.btnEliminarEntrenador.addActionListener(listener);
        vista.btnAltaJugador.addActionListener(listener);
        vista.btnModificarJugador.addActionListener(listener);
        vista.btnEliminarJugador.addActionListener(listener);
        vista.btnAsignarEntrenador.addActionListener(listener);
        vista.btnEliminarEntrenadorJugador.addActionListener(listener);
        vista.btnEliminarJugadorEntrenador.addActionListener(listener);
        vista.btnAnyadirJugadorEntrenador.addActionListener(listener);

        vistaLogin.btnLogin.addActionListener(listener);
        vistaLogin.btnRegistrar.addActionListener(listener);
        vistaLogin.btnSalir.addActionListener(listener);
    }
    @Override
    public void valueChanged(ListSelectionEvent e) {

        if (e.getValueIsAdjusting()) {
            if (e.getSource() == vista.listaEstadio && vista.listaEstadio.getSelectedValue()!=null) {
                estadio = (Estadio) vista.listaEstadio.getSelectedValue();
                mostrarEstadio(estadio);
            } else if (e.getSource() == vista.listaEquipo) {
                equipo = (Equipo) vista.listaEquipo.getSelectedValue();
                mostrarEquipo(equipo);
            } else if (e.getSource() == vista.listaEntrenador) {
                entrenador = (Entrenador) vista.listaEntrenador.getSelectedValue();
                mostrarEntrenador(entrenador);
                listarJugadoresEntrenador(entrenador);
                listarJugadoresDisponiblesParaEntrenador(entrenador);
            } else if (e.getSource() == vista.listaJugadores && vista.listaJugadores.getSelectedValue() != null) {
                jugador = (Jugador) vista.listaJugadores.getSelectedValue();
                mostrarJugador(jugador);
                listarEntrenadoresJugador(jugador);
                listarEntrenadoresDisponiblesParaJugador(jugador);

            }
        }

    }



    private void mostrarJugador(Jugador jugador) {
        vista.txtIdJugador.setText(""+jugador.getId());
        vista.txtNombreJugador.setText(jugador.getNombre());
        vista.txtDorsalJugador.setText(""+jugador.getDorsal());
        vista.dtFechaJugador.setDate(jugador.getFechaNacimiento().toLocalDate());
    }



    private void mostrarEntrenador(Entrenador entrenador) {
        vista.txtIdEntrenador.setText(""+entrenador.getId());
        vista.txtNombreEntrenador.setText(entrenador.getNombre());
        vista.txtAnyosTrabajando.setText(""+entrenador.getAnyosTrabajando());
        vista.cbEquipo.setSelectedItem(entrenador.getEquipo());
    }

    private void mostrarEstadio(Estadio estadio) {
        vista.txtIdEstadio.setText(""+estadio.getId());
        vista.txtNombreEstadio.setText(estadio.getNombre());
        vista.txtLocalidadEstadio.setText(estadio.getLocalidadEstadio());
        vista.dtFechaApertura.setDate(estadio.getFechaApertura().toLocalDate());
    }

    private void mostrarEquipo(Equipo equipo){
        vista.txtIdEquipo.setText(""+equipo.getId());
        vista.txtNombreEquipo.setText(equipo.getNombre());
        vista.txtLocalidadEquipo.setText(equipo.getLocalidadEquipo());
        vista.cbEstadio.setSelectedItem(equipo.getEstadio());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "Alta Estadio":
                modelo.altaEstadio(vista.txtNombreEstadio.getText(),vista.dtFechaApertura.getDate(),vista.txtLocalidadEstadio.getText());
                vaciarEstadio();
                break;
            case "Modificar Estadio":
                modificarEstadio(estadio);
                modelo.modificarEstadio(estadio);
                break;
            case "Eliminar Estadio":
                modelo.eliminarEstadio(estadio);
                break;
            case "Alta Equipo":
                modelo.altaEquipo(vista.txtNombreEquipo.getText(),vista.txtLocalidadEquipo.getText(), (Estadio) vista.dcbmEstadio.getSelectedItem());
                vaciarEquipo();
                break;
            case "Modificar Equipo":
                modificarEquipo(equipo);
                modelo.modificarEquipo(equipo);
                break;
            case "Eliminar Equipo":
                modelo.eliminarEquipo(equipo);
                break;
            case "Alta Entrenador":
                modelo.altaEntrenador(vista.txtNombreEntrenador.getText(),Integer.parseInt(vista.txtAnyosTrabajando.getText()), (Equipo) vista.dcbmEquipo.getSelectedItem());
                break;
            case "Modificar Entrenador":
                modificarEntrenador(entrenador);
                modelo.modificarEntrenador(entrenador);
                break;
            case "Eliminar Entrenador":
                entrenador = (Entrenador) vista.listaEntrenador.getSelectedValue();
                entrenador.desvincularTodos();
                modelo.eliminarEntrenador(entrenador);
                break;

            case "Alta Jugador":
                System.out.println(listaEntrenadores.size());
                modelo.altaJugador(vista.txtNombreJugador.getText(),Integer.parseInt(vista.txtDorsalJugador.getText()),vista.dtFechaJugador.getDate(),listaEntrenadores);
                listaEntrenadores= new ArrayList<>();
                break;
            case "Modificar Jugador":
                jugador = (Jugador) vista.listaJugadores.getSelectedValue();
                modificarJugador(jugador);
                modelo.modificarJugador(jugador);
                break;
            case "Eliminar Jugador":
                jugador = (Jugador) vista.listaJugadores.getSelectedValue();
                jugador.desvincularTodos();
                modelo.eliminarJugador(jugador);
                vaciarJugadores();
                break;
            case "EliminarJugadorEntrenado":
                Entrenador entrenador = (Entrenador) vista.listaEntrenador.getSelectedValue();
                entrenador.desvincularJugadores(vista.listaJugadoresEntrenados.getSelectedValuesList());
                modelo.modificarEntrenador(entrenador);
                break;
            case "AnyadirJugadorAEntrenar" :
                Entrenador entrenador1 = (Entrenador) vista.listaEntrenador.getSelectedValue();
                entrenador1.asignarJugadores(vista.listaJugadoresLibres.getSelectedValuesList());
                modelo.modificarEntrenador(entrenador1);
                break;

            case "AsignarEntrenadorAJugador":
                Jugador jugador = (Jugador) vista.listaJugadores.getSelectedValue();
                jugador.asignarEntrenadores(vista.listaEntrenadoresSinAsignar.getSelectedValuesList());
                modelo.modificarJugador(jugador);
                break;
            case "EliminarEntrenadorAsignado" :
                Jugador jugador1 = (Jugador) vista.listaJugadores.getSelectedValue();
                jugador1.desvincularEntrenadores(vista.listaEntrenadoresAsignados.getSelectedValuesList());
                modelo.modificarJugador(jugador1);
                break;
            case "Login":
                // si uno de los campos o ambos campos de la vistaLogin estan vacios salta una ventana emergente de error
                if(vistaLogin.txtContrasenya.getText().equalsIgnoreCase("") || vistaLogin.txtUsuario.getText().equalsIgnoreCase("")){
                    // se llama a la vista, accedemos al util y llamamos al mensaje de faltan datos,
                    // pasandole el texto que queremos mostrar en la ventana
                    vistaLogin.util.mensajeLoginFaltaDatos("Campos no completos, ingrese datos");
                }
                // si estan rellenos, pasamos a buscar el contenido
                else {
                    System.out.println(vistaLogin.listaUsuarios.getLista().size());
                    // realizamos un for segun el tamanyo que tiene la lista de usuarios que hay en la vistaLogin
                    for(int i = 0 ; i<vistaLogin.listaUsuarios.getLista().size(); i++ ) {
                        // si el texto que recogemos del caompo nombre de la vista coincide con un nombre
                        // de los que tiene un usuario de la lista
                        // hacemos que el boolean de usuario sea true;
                        if(vistaLogin.txtUsuario.getText().equalsIgnoreCase(vistaLogin.listaUsuarios.getLista().get(i).getUser())) {
                            // ponemos el boolean usuario en true
                            usuario = true;
                            // hacemos un if para comprobar si la contrasenya que recibimos del campo contrasenya de la vista
                            // es igual a la contrasenya del usuario que coincide con el nombre
                            if(vistaLogin.txtContrasenya.getText().equalsIgnoreCase(vistaLogin.listaUsuarios.getLista().get(i).getContrasena())) {
                                // si coincide hacemos que el boolean contrasenya sea true
                                usuario=true;
                                contrasena=true;
                                // hacemos que desaparezca la vista de login
                                vistaLogin.dispose();

                                // y hacemos visible la vista principal de la aplicacion
                                vista.frame.setVisible(true);

                            }
                            else {
                                if(!contrasena) {
                                    vistaLogin.util.mensajeLoginError("Contraseña erronea");
                                }
                            }
                        }
                    }
                    if(!usuario){
                        vistaLogin.util.mensajeLoginError("El usuario no es correcto");
                    }
                }

                break;
            // se encarga de dar un nuevo usuario a la aplicacion para acceder
            case "registrar":
                // se comprueba si hay contenido en los campos de login
                if(vistaLogin.txtContrasenya.getText().equalsIgnoreCase("") || vistaLogin.txtUsuario.getText().equalsIgnoreCase("")){
                    vistaLogin.util.mensajeLoginFaltaDatos("Campos no completos, ingrese datos");
                }
                else {
                    // se comprueba si el checkBox de admin esta seleccionado
                    if (vistaLogin.cbAdmin.isSelected()) {
                        // se crea un usuario admin
                        Usuario unUsuarioAdmin = new Usuario(vistaLogin.txtUsuario.getText(), vistaLogin.txtContrasenya.getText(), true);
                        vistaLogin.listaUsuarios.getLista().add(unUsuarioAdmin);
                        System.out.println(vistaLogin.listaUsuarios.getLista().size());
                    }
                    // si no lo esta se crea un usuario normal
                    else {
                        Usuario unUsuarioNormal1 = new Usuario(vistaLogin.txtUsuario.getText(), vistaLogin.txtContrasenya.getText(), false);
                        vistaLogin.listaUsuarios.getLista().add(unUsuarioNormal1);
                        for (int i = 0; i < vistaLogin.listaUsuarios.getLista().size(); i++) {
                            System.out.println(vistaLogin.listaUsuarios.getLista().get(i).toString());
                        }
                    }
                }
                break;
            // sirve para cerrar la aplicacion en caso de no querer logearse
            case "salir":
                System.exit(0);
                break;

        }
        listarEstadiosComboBox();
        listarEquiposComboBox();
        listarEstadios();
        listarEquipos();
        vaciarJugadores();
        vaciarEntrenadores();

    }
    private void listarEntrenadoresJugador(Jugador jugador){
        Set<Entrenador> entrenadores = jugador.getListaEntrenadores();
        vista.dlmEntrenadorAsignado.clear();
        for(Entrenador entrenador : entrenadores){
            vista.dlmEntrenadorAsignado.addElement(entrenador);
        }
    }
    private void listarEntrenadoresDisponiblesParaJugador(Jugador jugador){
        List<Entrenador> entrenadores = modelo.obtenerEntrenadoresDisponiblesParaElJugador(jugador);
        vista.dlmEntrenadorSinAsignar.clear();
        for(Entrenador entrenador : entrenadores){
            vista.dlmEntrenadorSinAsignar.addElement(entrenador);
        }
    }

    private void listarJugadoresEntrenador(Entrenador entrenador) {
        Set<Jugador> jugadores = entrenador.getListaJugadores();
        vista.dlmJugadorAsignado.clear();
        for(Jugador jugador : jugadores){
            vista.dlmJugadorAsignado.addElement(jugador);
        }
    }

    private void listarJugadoresDisponiblesParaEntrenador(Entrenador entrenador){
        List<Jugador> jugadores = modelo.obtenerJugadoresDisponiblesParaElEntrenador(entrenador);
        vista.dlmJugadorSinAsignar.clear();
        for(Jugador jugador : jugadores){
            vista.dlmJugadorSinAsignar.addElement(jugador);
        }
    }

    private void vaciarJugadores() {
        vista.txtIdJugador.setText("");
        vista.txtNombreJugador.setText("");
        vista.txtDorsalJugador.setText("");
        vista.dtFechaJugador.clear();
        vista.dlmEntrenadorAsignado.clear();
        vista.dlmEntrenadorSinAsignar.clear();
        listarJugadores();
    }

    private void vaciarEntrenadores() {
        vista.txtIdEntrenador.setText("");
        vista.txtNombreEntrenador.setText("");
        vista.txtAnyosTrabajando.setText("");
        vista.cbEquipo.setSelectedIndex(0);
        vista.dlmJugadorSinAsignar.clear();
        vista.dlmJugadorAsignado.clear();
        listarEntrendores();
    }

    private void vaciarEstadio() {
        vista.txtIdEstadio.setText("");
        vista.txtNombreEstadio.setText("");
        vista.txtLocalidadEstadio.setText("");
        vista.dtFechaApertura.clear();
    }
    private void vaciarEquipo(){
        vista.txtIdEquipo.setText("");
        vista.txtNombreEquipo.setText("");
        vista.txtLocalidadEquipo.setText("");
        vista.dcbmEquipo.setSelectedItem(null);
    }
    private void modificarEquipo(Equipo equipo) {
        equipo.setNombre(vista.txtNombreEquipo.getText());
        equipo.setEstadio((Estadio) vista.dcbmEstadio.getSelectedItem());
        equipo.setLocalidadEquipo(vista.txtLocalidadEquipo.getText());
    }
    private void modificarEstadio(Estadio estadio) {
        estadio.setNombre(vista.txtNombreEstadio.getText());
        estadio.setFechaApertura(Date.valueOf(vista.dtFechaApertura.getDate()));
        estadio.setLocalidadEstadio(vista.txtLocalidadEstadio.getText());
    }
    private void modificarEntrenador(Entrenador entrenador) {
        entrenador.setAnyosTrabajando(Integer.parseInt(vista.txtAnyosTrabajando.getText()));
        entrenador.setNombre(vista.txtNombreEntrenador.getText());
        entrenador.setEquipo((Equipo) vista.cbEquipo.getSelectedItem());
    }

    public void modificarJugador(Jugador jugador){
        jugador.setNombre(vista.txtNombreJugador.getText());
        jugador.setDorsal(Integer.parseInt(vista.txtDorsalJugador.getText()));
        jugador.setFechaNacimiento(Date.valueOf(vista.dtFechaJugador.getDate()));

    }


    private void listarEstadiosComboBox(){
        vista.dcbmEstadio.removeAllElements();
        vista.dcbmEstadio.addElement(null);
        for (Estadio estadio:modelo.getEstadios()) {
            vista.dcbmEstadio.addElement(estadio);
        }
    }


    private void listarEquiposComboBox(){
        vista.dcbmEquipo.removeAllElements();
        vista.dcbmEquipo.addElement(null);
        for (Equipo equipo :modelo.getEquipos()) {
            vista.dcbmEquipo.addElement(equipo);
        }
    }

    private void listarJugadores() {
        vista.dlmJugador.removeAllElements();
        for (Jugador jugador: modelo.getJugadores()) {
            vista.dlmJugador.addElement(jugador);
        }
    }


    private void listarEstadios(){
        vista.dlmEstadio.removeAllElements();
        for (Estadio estadio: modelo.getEstadios()) {
            vista.dlmEstadio.addElement(estadio);
        }
    }

    private void listarEquipos(){
        vista.dlmEquipo.removeAllElements();
        for (Equipo equipo: modelo.getEquipos()) {
            vista.dlmEquipo.addElement(equipo);
        }
    }

    private void  listarEntrendores(){
        vista.dlmEntrenador.removeAllElements();
        for (Entrenador entrenador: modelo.getEntrenadores()) {
            vista.dlmEntrenador.addElement(entrenador);
        }
    }


    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        modelo.desconectar();
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    private class Actualizar extends Thread{

        @Override
        public void run() {

            while (true) {
                try {
                    sleep(25000);
                    listarEstadiosComboBox();
                    listarEquiposComboBox();
                    listarEstadios();
                    listarEquipos();
                    vaciarJugadores();
                    vaciarEntrenadores();
                    System.out.println("comprobacion realizada correctamente");

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
