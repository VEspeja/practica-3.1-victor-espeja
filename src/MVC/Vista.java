package MVC;

import base.Entrenador;
import base.Equipo;
import base.Estadio;
import base.Jugador;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

public class Vista {
    public JFrame frame;
    public JPanel panel1;
    public JTextField txtIdEstadio;
    public JTextField txtNombreEstadio;
    public JTextField txtLocalidadEstadio;
    public JList listaEstadio;
    public JButton btnAltaEstadio;
    public JButton btnModificarEstadio;
    public JButton btnEliminarEstadio;
    public JTextField txtIdEquipo;
    public JTextField txtNombreEquipo;
    public JTextField txtLocalidadEquipo;
    public JList listaEquipo;
    public JButton btnAltaEquipo;
    public JButton btnModificarEquipo;
    public JButton btnEliminarEquipo;
    public JComboBox cbEstadio;
    public DatePicker dtFechaApertura;
    public JTextField txtIdEntrenador;
    public JTextField txtNombreEntrenador;
    public JTextField txtAnyosTrabajando;
    public JComboBox cbEquipo;
    public JList listaEntrenador;
    public JButton btnAltaEntrenador;
    public JButton btnModificarEntrenador;
    public JButton btnEliminarEntrenador;
    public JTextField txtIdJugador;
    public JTextField txtNombreJugador;
    public JTextField txtDorsalJugador;
    public DatePicker dtFechaJugador;
    public JList listaJugadores;
    public JButton btnAltaJugador;
    public JButton btnModificarJugador;
    public JButton btnEliminarJugador;
    public JList listaJugadoresEntrenados;
    public JButton btnEliminarJugadorEntrenador;
    public JButton btnAnyadirJugadorEntrenador;
    public JList listaJugadoresLibres;
    public JList listaEntrenadoresAsignados;
    public JList listaEntrenadoresSinAsignar;
    public JButton btnEliminarEntrenadorJugador;
    public JButton btnAsignarEntrenador;
    private JTabbedPane tabbedPane1;

    public DefaultListModel<Estadio> dlmEstadio;
    public DefaultListModel<Equipo> dlmEquipo;
    public DefaultListModel<Entrenador> dlmEntrenador;
    public DefaultListModel<Entrenador> dlmEntrenadorJugador;
    public DefaultListModel<Jugador> dlmJugador;
    public DefaultComboBoxModel<Estadio> dcbmEstadio;
    public DefaultComboBoxModel<Equipo> dcbmEquipo;
    public DefaultListModel<Entrenador> dlmEntrenadorSinAsignar;
    public DefaultListModel<Entrenador> dlmEntrenadorAsignado;
    public DefaultListModel<Jugador> dlmJugadorSinAsignar;
    public DefaultListModel<Jugador> dlmJugadorAsignado;

    VistaLogin vistaLogin;

    public Vista(VistaLogin unaVistaLogin) {
        frame = new JFrame("Vista");
        // asigno a el objeto vistaLogin el objeto que recibe
        vistaLogin = unaVistaLogin;
        // declaro el JDialog de la clase VistaLogin, haciendo que esta clase sea la padre, y que sea modal
        vistaLogin.vistaLogin = new JDialog(frame, "ventana login", true);
        // el metodo init se engarga de dar las propiedades de la ventana

        init(frame);
    }

    private void init(JFrame frame){
        // anyadimos un titulo a la ventana
        frame.setTitle("Equipos");
        frame.setContentPane(panel1);
        // damos dimensiones a la ventana
        frame. setSize(800, 600);
        // la hacemos visible
        frame.setVisible(true);
        // le asignamos la caracteristica de cuando se cierre finaliza el programa
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // colocamos la ventana en el centro de la pantalla
        frame.setLocationRelativeTo(null);
        // le anyadimos al panel principal un borderLayout
        frame.getContentPane().setLayout(new BorderLayout(0, 0));
        // este metodo se encarga de que se inicialicen los Default ... Model que vamos a usar
        iniciarModelos();
    }

    private void iniciarModelos() {
        dlmEquipo = new DefaultListModel<>();
        listaEquipo.setModel(dlmEquipo);

        dlmEstadio = new DefaultListModel<>();
        listaEstadio.setModel(dlmEstadio);

        dlmEntrenador = new DefaultListModel<>();
        listaEntrenador.setModel(dlmEntrenador);

        dlmJugador = new DefaultListModel<>();
        listaJugadores.setModel(dlmJugador);

        dcbmEstadio = new DefaultComboBoxModel<>();
        cbEstadio.setModel(dcbmEstadio);

        dcbmEquipo = new DefaultComboBoxModel<>();
        cbEquipo.setModel(dcbmEquipo);

        dlmJugadorAsignado= new DefaultListModel<>();
        listaJugadoresEntrenados.setModel(dlmJugadorAsignado);

        dlmJugadorSinAsignar = new DefaultListModel<>();
        listaJugadoresLibres.setModel(dlmJugadorSinAsignar);

        dlmEntrenadorAsignado = new DefaultListModel<>();
        listaEntrenadoresAsignados.setModel(dlmEntrenadorAsignado);

        dlmEntrenadorSinAsignar = new DefaultListModel<>();
        listaEntrenadoresSinAsignar.setModel(dlmEntrenadorSinAsignar);
    }


}
