package MVC;

import base.Entrenador;
import base.Equipo;
import base.Estadio;
import base.Jugador;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.hibernate.service.ServiceRegistry;
import principal.HibernateUtil;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Modelo {
    private static SessionFactory sessionFactory;
    private static Session session;
    HibernateUtil hibernate;

    public Modelo(){
        hibernate = new HibernateUtil();
    }

    public void conectar(){

         hibernate.buildSessionFactory();
        hibernate.openSession();
        session = hibernate.getCurrentSession();
    }

    public void desconectar(){
        hibernate.closeSessionFactory();
    }

    public void altaEstadio(String nombre, LocalDate fechaApertura, String localidad) {
        Estadio estadio = new Estadio(nombre,fechaApertura,localidad);
        session.beginTransaction();
        System.out.println(estadio);
        session.save(estadio);
        session.getTransaction().commit();
    }
    public void modificarEstadio(Estadio estadio){
        session.beginTransaction();
        session.saveOrUpdate(estadio);
        session.getTransaction().commit();

    }
    public void eliminarEstadio(Estadio estadio){
        session.beginTransaction();
        session.delete(estadio);
        session.getTransaction().commit();
    }

    public void altaEquipo(String nombre, String localidad, Estadio estadio) {
        Equipo equipo = new Equipo(nombre,localidad,estadio);
        estadio.getListaEquipos().add(equipo);
        session.beginTransaction();
        session.save(equipo);
        session.getTransaction().commit();
    }

    public void modificarEquipo(Equipo equipo){
        session.beginTransaction();
        session.saveOrUpdate(equipo);
        session.getTransaction().commit();

    }
    public void eliminarEquipo(Equipo equipo){
        session.beginTransaction();
        session.delete(equipo);
        session.getTransaction().commit();
    }

    public void altaEntrenador(String nombre, int anyosTrabajando, Equipo equipo) {
        Entrenador entrenador;
        if(equipo !=null) {
           entrenador = new Entrenador(nombre, anyosTrabajando, equipo);
            equipo.getListaEntrenadores().add(entrenador);
        }
        else{
             entrenador = new Entrenador(nombre,anyosTrabajando);
            }
        session.beginTransaction();
        session.save(entrenador);
        session.getTransaction().commit();
    }
    public void modificarEntrenador(Entrenador entrenador){
        session.beginTransaction();
        session.saveOrUpdate(entrenador);
        session.getTransaction().commit();

    }
    public void eliminarEntrenador(Entrenador entrenador){
        session.beginTransaction();
        session.delete(entrenador);
        session.getTransaction().commit();
    }



    public void altaJugador(String nombre, int dorsal, LocalDate fechaNacimiento, ArrayList<Entrenador> listaEntrenadores) {
        Jugador jugador = new Jugador(nombre,dorsal,fechaNacimiento);
        session.beginTransaction();
        session.save(jugador);
        session.getTransaction().commit();
    }

    public void modificarJugador(Jugador jugador){
        session.beginTransaction();
        session.saveOrUpdate(jugador);
        System.out.println(jugador.getListaEntrenadores());
        session.getTransaction().commit();

    }
    public void eliminarJugador(Jugador jugador){
        session.beginTransaction();
        session.delete(jugador);
        session.getTransaction().commit();
    }
    public List<Entrenador> obtenerEntrenadoresDisponiblesParaElJugador(Jugador jugador) {
        List<Entrenador> lista = getEntrenadores();
        lista.removeAll(jugador.getListaEntrenadores());
        return lista;
    }
    public List<Jugador> obtenerJugadoresDisponiblesParaElEntrenador(Entrenador entrenador) {
        List<Jugador> lista = getJugadores();
        lista.removeAll(entrenador.getListaJugadores());
        return lista;
    }

    public List<Estadio> getEstadios(){
        Query query = session.createQuery("FROM Estadio");
        List<Estadio> lista = query.getResultList();
        return lista;
    }


    public List<Equipo> getEquipos(){
        Query query = session.createQuery("FROM Equipo");
        List<Equipo> lista = query.getResultList();
        return lista;
    }

    public List<Entrenador> getEntrenadores(){
        Query query = session.createQuery("FROM Entrenador");
        List<Entrenador> lista = query.getResultList();
        return lista;
    }

    public List<Jugador> getJugadores(){
        Query query = session.createQuery("FROM Jugador");
        List<Jugador> lista = query.getResultList();
        return lista;
    }


}
