package usuarios;

import java.util.ArrayList;

public class ListaUsuarios {
	private ArrayList<Usuario> listaUsers;

	public ListaUsuarios() {
		// se inicializa el array en el constructor
		this.listaUsers = new ArrayList<>();
		// se crea un metodo con el que creamos los usuarios con los que poder operar al declarar una Lista de Usuarios
		anyadirUsuarios();
	}

	// metodo que se utiliza para añadir al array de Usuarios unos que hemos diseñado
	private void anyadirUsuarios() {
		// se declara un usuario admin y otro normal
		Usuario usuarioNormal = new Usuario("victor","1234",false);
		Usuario administrador = new Usuario("admin","1111",true);
		// se añaden al array
		this.listaUsers.add(usuarioNormal);
		this.listaUsers.add(administrador);
	}
	// metodo encargado de recibir unos parametros y crear un usuario que sera añadido al array
	private void nuevoUser(String nombre, String contrasena, boolean admin) {
		Usuario unUsuario = new Usuario(nombre, contrasena, admin);
		listaUsers.add(unUsuario);
	}

	public ArrayList<Usuario> getLista() {
		return listaUsers;
	}

	public void setLista(ArrayList<Usuario> lista) {
		this.listaUsers = lista;
	}
	
	
	
	
	
}
