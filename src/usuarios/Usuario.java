package usuarios;

public class Usuario {
	private String user;
	private String contrasena;
	private boolean admin;

	/**
	 * constructor de la clase que recibe como paramtros para asignar los valores al objeto
	 * @param nombre de tipo String
	 * @param contrasena de tipo String
	 * @param admin de tipo boolean
	 */
	public Usuario(String nombre, String contrasena, boolean admin) {
		
		this.user = nombre;
		this.contrasena = contrasena;
		this.admin = admin;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	@Override
	public String toString() {
		return "Usuario [user=" + user + ", contraseña=" + contrasena + ", admin=" + admin + "]";
	}
	
	
	
}
